# Objectives:

## Introduction to Identity Provider - Kratos
[Docs OpenSource](https://www.ory.sh/docs/hydra/self-hosted/production)

### Basic Setup
#### Service
#### UI
####

### Email Template
#### Registration Email Validation

### Hooks

#### Registration - before
#### Registration - after

#### Login - before
#### Login - after


## Introduction to OAuth2 Provider - Hydra
[Docs OpenSource](https://www.ory.sh/docs/hydra/self-hosted/production)

## Introduction to Permission (ACL, RBAC) Provider - Keto
[Docs OpenSource](https://www.ory.sh/docs/hydra/self-hosted/production)
